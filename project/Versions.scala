object Versions {
  val http4s = "0.18.17"

  val scalaTags = "0.6.7"
  val akkaJs = "1.2.5.15"

  val config = "1.3.1"
  val log4j = "2.11.0"
  val scalatest = "3.0.4"
}