package client

import akka.actor.ActorSystem
import akka.util.Timeout
import client.component.Layout.GetReady
import client.component.Menu.MenuItem
import client.component.{Layout, Menu}
import org.scalajs.dom.{Event, window}

import scala.concurrent.duration._

/**
  * @author Anton Gnutov
  */
object Application {

  val system: ActorSystem = ActorSystem("application")

  private def onPageLoaded(e: Event): Unit = {
    val menuItems = Seq(
      MenuItem("Home", "#home"),
      MenuItem("About", "#about")
    )
    val menu = system.actorOf(Menu.props("Akka.js sample", menuItems), "Menu")
    val layout = system.actorOf(Layout.props(menu), "Layout")

    import akka.pattern.ask
    import system.dispatcher
    implicit val timeout: Timeout = Timeout(10.seconds)
    (layout ? GetReady).foreach { _ =>
      system.actorOf(Router.props(layout, menu), "Router")
    }
  }

  def main(args: Array[String]): Unit = {
    window.onload = onPageLoaded _
  }
}
