package client

import akka.actor.{Actor, ActorRef, Props}
import client.Router.HashChanged
import client.component.Layout.{AboutPage, HomePage}
import client.component.Menu.RouteChanged
import org.scalajs.dom._

/**
  * @author Anton Gnutov
  */
class Router(layout: ActorRef, menu: ActorRef) extends Actor {
  window.onhashchange = (_: HashChangeEvent) => {
    self ! HashChanged(window.location.hash)
    menu ! RouteChanged
  }

  override def preStart(): Unit = {
    self ! HashChanged(window.location.hash)
  }

  override def receive: Receive = {
    case HashChanged(hash) =>
      hash match {
        case "" | "#home" => layout ! HomePage
        case "#about"     => layout ! AboutPage
      }
  }
}

object Router {
  def props(layout: ActorRef, menu: ActorRef): Props = Props(new Router(layout, menu))

  case class HashChanged(hash: String)
}