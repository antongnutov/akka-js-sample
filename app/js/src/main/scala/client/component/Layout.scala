package client.component

import akka.actor.{Actor, ActorRef, Props}
import client.component.Layout._
import org.scalajs.dom.html.Element
import org.scalajs.dom.{Node, document}

import scalatags.JsDom.all._

/**
  * @author Anton Gnutov
  */
class Layout(menu: ActorRef) extends Actor {
  private val about = context.actorOf(About.props("Akka.js sample"), "About")
  private val jokes = context.actorOf(Jokes.props(), "Jokes")

  private var application: Option[ActorRef] = None

  def bodyAppend(nodeToAppend: Node): Node = document.body.appendChild(nodeToAppend)

  override def preStart(): Unit = menu ! Render

  override def receive: Receive = {
    case HomePage =>
      jokes ! Render

    case AboutPage =>
      about ! Render

    case MenuRendered(element) =>
      bodyAppend(element.render)
      bodyAppend(
        div(cls := "splash-container",
          div(id := "container", cls := "splash")
        ).render
      )
      application.foreach(_ ! Ready)

    case BodyRendered(element) =>
      val container = document.getElementById("container")
      container.innerHTML = ""
      container.appendChild(element.render)

    case GetReady =>
      application = Some(sender())
  }
}

object Layout {
  def props(menu: ActorRef): Props = Props(new Layout(menu))

  sealed trait Page
  case object HomePage extends Page
  case object AboutPage extends Page

  case object Render
  case class BodyRendered(element: Element)
  case class MenuRendered(element: Element)

  case object GetReady
  case object Ready
}